<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Oowlish');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',RR%n-$aGujsu<##]ruBO)o8Cg7[4cK=%x^%nN<MQ)<OUKd)L)EK#pqLdX/<i[D~');
define('SECURE_AUTH_KEY',  'Zz;U(9b]wgLRn7BFn?)[/;qYAf&^RE3b?G5fjQ1]L8>39;UM;hr&[A31deE6p5Ya');
define('LOGGED_IN_KEY',    '5jXZ`JW+ASIRiRKQ@=B~[.iu:Y2?_&PIg6h<FFP.yo#`)Q1>?iM-5F3#)v:.tsFS');
define('NONCE_KEY',        'RZTFeYt#*80([Kw0R!tLo1i/Az;j}paXb#HXiAoEuuqKJ,kA,a]TD$4ZR:B(>:]6');
define('AUTH_SALT',        'KT:/M)dtf[PCrKu@^h3NP*=kJbpq~HZ4lXNBnv8-2~-pDf(Rm+Z2JZq Q%}9c}_g');
define('SECURE_AUTH_SALT', 'fc$E_BpIB(_Cl$KbUw7G&@c{wkVE4_0SelEuw`HJF[0q_}!B^?[w!#Ar?-|51a>,');
define('LOGGED_IN_SALT',   'e%6w/3KmA1|MUB_XNEZZYaI^o$fX{pye9%^|.vc%1Cz]6rm8f0w9_2r4j{xwYd+?');
define('NONCE_SALT',       'mT_H4(Yoaz]nhnC!,BPY|,g&j68WGe9WVT4(iWTS?}^(?B0P=Z#d 1kj`^TWjoea');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
