<?php
/* enqueue script for parent theme stylesheeet */        
function childtheme_parent_styles() {
 
 // enqueue style
 wp_enqueue_style( 'parent', get_template_directory_uri().'/style.css' );                       
}
add_action( 'wp_enqueue_scripts', 'childtheme_parent_styles');


  // adição de código custom post type slider
  function custom_post_type_racas() {
	register_post_type('racas', array(
		'label' => 'racas',
		'description' => 'Raça',
		'public' => true,
		'menu_icon'   => 'dashicons-products',
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'racas', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats'),
			'taxonomies' => array('category'), // <=== habilita o uso de categorias por default

			'labels' => array (
				'name' => 'Raça',
				'singular_name' => 'Raça',
				'menu_name' => 'Raça',
				'add_new' => 'Adicionar Novo',
				'add_new_item' => 'Adicionar Novo Raça',
				'edit' => 'Editar',
				'edit_item' => 'Editar Raça',
				'new_item' => 'Novo Raça',
				'view' => 'Ver Raça',
				'view_item' => 'Ver Raça',
				'search_items' => 'Procurar Raça',
				'not_found' => 'Nenhum Raça Encontrada',
				'not_found_in_trash' => 'Nenhum Raça Encontrada no Lixo',
			)
		));
}
add_action('init', 'custom_post_type_racas');


