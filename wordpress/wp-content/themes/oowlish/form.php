<?php /* Template Name: formulario */  ?>
<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="title-h1 mt-4 mb-4 text-center">Relate sua experiência<br/>
          <span class="lead">Com uma das raças cadastradas</span></h1>
          <div id="preloader" class="alert text-center" 
          style="display: none;">
          <img src="wp-content/themes/oowlish/assets/images/preloader.gif" align="absmiddle"> <br> <small><i>Aguarde, cadastrando sua experiência...</i></small> <br><br>
          </div>
        <div id="contato_form_sucesso" class="alert alert-success" style="display: none;"> Sua experiência foi cadastrada com sucesso! </div>
        <form class="form-all" id="demo-form" action="wp-content/themes/oowlish/savebank.php" method="post">
          <div class="row">
            <div class="col-sm-12">
              <label for="nome">Nome*</label>
              <input type="text" class="form-control input-cnt" id="nome" name="nome" required="true">
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <select name="raca" id="nome_raca" class="form-control">
                     <?php $args = array (
                                'post_type' => 'racas',
                                'order'   => 'DESC',
                                'posts_per_page'   => '6',
                            );
                            $the_query = new WP_Query ( $args );
                        ?>
                      <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <option value="<?php the_field('nome_da_raca'); ?>" ><?php the_field('nome_da_raca'); ?></option>

                <?php endwhile; else: endif; ?>
                <?php wp_reset_query(); wp_reset_postdata(); ?>

              </select>
            </div>
            <div class="col-sm-6">
              <label for="email">Email*</label>
              <input type="email" class="form-control input-cnt" name="email" id="email" aria-describedby="emailHelp" required="true">
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <label>Experiência*</label>
              <textarea class="form-control input-cnt" name="areatext" id="areatext" rows="5"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12" align="center">
              <!-- <input type="hidden" name="token" value="<?php echo $token; ?>"> -->
              <button type="submit" class="btn btn-primary envio-mail lato-light" onclick="EnviaDados();">Enviar</button>
          </div>
        </div>
        <!-- criar função -->
      </form>
    </div>
  </div>
</div>
</section>