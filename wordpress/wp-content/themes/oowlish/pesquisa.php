public function tableUsuarioUM() {
    $consulta = PDOUtil::getStance()->prepare("SELECT pes.id_pessoa, pes.nome, pes.cpf, end.id_endereco, end.descricao, end.complemento, 
        end.cep,cid.id,cid.nome,cid.id_estado,est.id, est.nome, est.uf, est.id_pais FROM pessoa pes 
        INNER JOIN endereco end ON (pes.id_endereco = end.id_endereco)
        INNER JOIN cidade cid ON (end.id_endereco = cid.id)
        INNER JOIN estado est ON (cid.id = est.id)");
    $consulta->execute();
    while ($linha = $consulta->fetch(PDO::FETCH_OBJ)) { 
            echo '<thead>';
            echo '<tr>';
                echo '<td>'. $linha->nome. '</td>';
                echo '<td>'.$linha->cpf.'</td>';
                echo ' <td><a class="btn btn-primary" href="index.php?pagina=cadastrarUsuarios&id='.$linha->id_pessoa.'&nome='.$linha->nome.'&id_endereco='.$linha->id_endereco.'&cpf='.
                        $linha->cpf.'">Editar</a>';
                echo ' <a class="btn btn-danger" id="btn-apagar" href="index.php?pagina=../controller/controllerUser&id='
                .$linha->id_pessoa.'&nome='.$linha->nome.'&id_endereco='.$linha->id_endereco.'&acao=deletar">Deletar</a></td>';
            echo '</tr>';
            echo '</thead>';
    }
}