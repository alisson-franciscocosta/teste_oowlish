<?php
/* Template Name: pagehome */ 
get_header(); 
 ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="accordion" id="accordionExample">
          

          <?php
            $args = array (
            'post_type' => 'racas',
            'order'   => 'DESC',
            'posts_per_page'   => '8',
            );
            $the_query = new WP_Query ( $args );
          ?>
          <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

          <div class="card">
            <div class="card-header" id="heading<?php the_field('numero_heading'); ?>">
              <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php the_field('numero_colapse'); ?>" aria-expanded="true" aria-controls="collapse<?php the_field('numero_colapse'); ?>">
                  <?php the_field('nome_da_raca');?>
                  </button>
              </h2>
            </div>

            <div id="collapse<?php the_field('numero_colapse'); ?>" class="collapse show" aria-labelledby="heading<?php the_field('numero_heading'); ?>" data-parent="#accordionExample">
              <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                   <h3>
                     Detalhes da raça
                   </h3>
                   <p class="mt-3">
                      <?php the_field('detalhe'); ?>
                   </p>
                 </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                   <h3 class="text-left title-h3">
                     <small>Comentários da raça</small>
                   </h3>

                   <?php 
                    require_once("connectionbank.php");

                    $sql = $conexao->prepare("SELECT * FROM wp_posts");
                    $sql->execute();
                    ?>

                   <div class="experiencia">
                    <?php 
                    while($ln2 = $sql->fetchObject()){

                      $nome_post = $ln2->post_title;
                      
                      $result = $conexao->prepare("SELECT * FROM registro_racas");
                      $result->execute();
                      while($ln = $result->fetchObject()){
                      
                      $nome_raca = $ln->raca;

                      if ($nome_raca = $nome_post) {
                       echo '<h4 class="nome_pai">'.$ln->nome.'</h4>';
                       echo '<p>'.$ln->mensagem.'</p>';
                      } else{
                        echo '<h4 class="nome_pai">não há registro</h4>';
                      }
                       
                    }
                  }
                     ?>
                   </div>
                      
                  </div>
               </div> 
              </div>
            </div>
          </div>

          <?php endwhile; else: endif; ?>
          <?php wp_reset_query(); wp_reset_postdata(); ?>

        </div>
			</div>
		</div>
	</div>
</section>






<!-- Limitador de acesso do formulário -->
<?php if ( is_user_logged_in() ) {
    require_once("form.php");  
  } else{ ?>

    <div align="center">
      <?php  
        echo "<h3 style='font-size:15px;'>Você não está logado para adicionar comentários às raças</h3>";
        echo '<a href="http://localhost/teste_oowlish/wordpress/register/">Registrar</a>';
        echo '<a href="http://localhost/teste_oowlish/wordpress/login/">Entrar</a>';
      ?>
    </div>
    <?php
      }
    ?>





<?php get_footer(); ?>