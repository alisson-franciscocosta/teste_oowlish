! [Oowlish Logo] (oowlish.jpg)

# WordPress / PHP Engineer

Dear recruiter,

I am very happy to have reached this point in selecting the PHP engineer position, I carried out the test with simple solutions and intuitive interface.
Follow the instructions to run the code.

## Instructions

- After giving a Git clone in the desired folder, just go to your local server and import the "oowlish.sql" database;
- Now access the website hosted on a php server;
- ADM user is "alisson" and password "minhaenha123";
- If you are not logged in to any user, the home page will show only registered breeds and clicking on them, will show a comment and some experience sent by a dog father;
- If you are logged in, a form will appear where you can add experiences with a selected breed;
- it is possible to create a register or log in still on the main page.
- The theme is Son of `Twenty Nineteen`.

Thank you very much and I hope I can go further!